import java.util.concurrent.TimeUnit;

public class ParkingLot {
    private static java.util.concurrent.Semaphore semaphore = null;

    public ParkingLot(int capacity) {
        semaphore = new java.util.concurrent.Semaphore(capacity);
    }

    public boolean getFreeSpot(int maxWaitTime) {
        try {
            return semaphore.tryAcquire(maxWaitTime, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isFull() { return semaphore.availablePermits() == 0; }

    public void releaseSpot() {
        semaphore.release();
    }
}
