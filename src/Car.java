import java.util.Random;

public class Car extends Thread {
    private final String name;
    private final ParkingLot parkingLot;
    private static final Random random = new Random();
    private final int MAX_WAIT_TIME_SEC = random.nextInt(5) + 1;
    private final int STAND_TIME_MS = random.nextInt(5000) + 1000;

    public Car(String name, ParkingLot parkingLot) {
        this.name = name;
        this.parkingLot = parkingLot;
    }

    @Override
    public void run() {
        if(parkingLot.isFull())
            System.out.println(name + " is waiting " + MAX_WAIT_TIME_SEC + " seconds for a free space.");

        if(!parkingLot.getFreeSpot(MAX_WAIT_TIME_SEC)) {
            System.out.println(name + " could not find a spot and left");
            return;
        }

        System.out.println(name + " parked at spot");
        try {
            Thread.sleep(STAND_TIME_MS); // Імітація паркування
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println(name + " left spot");
            parkingLot.releaseSpot();
        }
    }
}
