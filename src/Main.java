public class Main {
    public static void main(String[] args) {
        int capacity = 5; // Number of parking spaces
        int carQuantity = 10; // Number of cars
        var parkingLot = new ParkingLot(capacity);

        for (int i = 0; i < carQuantity; i++) {
            Car car = new Car("Car" + i, parkingLot);
            car.start();
        }
    }
}
